#ifndef TRANSFORMATION_ROTATE_TRANSFORMER_H
#define TRANSFORMATION_ROTATE_TRANSFORMER_H

#include "image.h"

struct image rotate_transform(struct image const source);

#endif
