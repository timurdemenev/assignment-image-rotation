#ifndef FILE_FILE_H
#define FILE_FILE_H

#include <stdio.h>

FILE* file_open(char* file_name, char* props);

void file_close(FILE* file);

int file_seek(FILE* file, long int offset, int origin);

#endif
