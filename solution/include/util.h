#include "bmp.h"

const char* get_message_for_image_read_status(enum read_status status);

const char* get_message_for_image_write_status(enum write_status status);
