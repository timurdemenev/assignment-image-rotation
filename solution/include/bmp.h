#ifndef FORMAT_BMP_H
#define FORMAT_BMP_H

#include <stdint.h>
#include <stdio.h>

#include "image.h"

enum read_status {
  READ_OK = 0,
  READ_ERROR,
  READ_INVALID_FILE,
  READ_INVALID_SIGNATURE,
  READ_INVALID_BITS,
  READ_INVALID_HEADER,
  READ_INVALID_WIDTH,
  READ_INVALID_HEIGHT,
  READ_UNKNOWN_ERROR  
};

enum read_status from_bmp(FILE* img, struct image* image_ptr);

enum  write_status  {
  WRITE_OK = 0,
  WRITE_ERROR,
  WRITE_UNKNOWN_ERROR
};

enum write_status to_bmp(FILE* img, struct image const* image_ptr);

#endif
