#ifndef FILE_FILE_IN_H
#define FILE_FILE_IN_H

#include <inttypes.h>
#include <stdio.h>

size_t file_read(void* dest_ptr, size_t block_size, size_t num_of_blocks, FILE* f);

#endif
