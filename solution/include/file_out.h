#ifndef FILE_FILE_OUT_H
#define FILE_FILE_OUT_H

#include <inttypes.h>
#include <stdio.h>

size_t file_write(void* dest_ptr, size_t block_size, size_t num_of_blocks, FILE* f);

#endif
