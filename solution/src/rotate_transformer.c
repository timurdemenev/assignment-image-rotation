#include "rotate_transformer.h"

#include <inttypes.h>
#include <stddef.h>
#include <stdlib.h>

#include "image.h"

static struct pixel* pixel_by_coordinates(struct pixel* pixels, const size_t x, const size_t y, const uint64_t width) {
	return &pixels[y * width + x];
} 

struct image rotate_transform(struct image const source) {
	struct image target = {0};

	if (source.data != NULL) {
		const uint32_t width = source.width;
		const uint32_t height = source.height;
		
		target = create_image(height, width);
		
		if (target.data != NULL) {
			for (size_t j = 0; j < height; ++j) {
				for (size_t i = 0; i < width; ++i) {
					*pixel_by_coordinates(target.data, height - j - 1, i, height) = *pixel_by_coordinates(source.data, i, j, width);
				}
			}
		}
	}
	
	return target;
}
