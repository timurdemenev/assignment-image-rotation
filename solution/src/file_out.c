#include "file_out.h"

#include <inttypes.h>
#include <stdio.h>

size_t file_write(void* dest_ptr, size_t block_size, size_t num_of_blocks, FILE* f) {
	return fwrite(dest_ptr, block_size, num_of_blocks, f);
}
