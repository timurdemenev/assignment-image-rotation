#include "image.h"

#include <stdlib.h>

struct image create_image(const uint64_t width, const uint64_t height) {
	return (struct image) {.width = width, .height = height, .data = malloc(sizeof(struct pixel) * width * height)};
}

void free_image(struct image* image) {
	if (image != NULL)
		free(image->data);
}
