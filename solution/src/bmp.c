
#include "bmp.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "file.h"
#include "file_in.h"
#include "file_out.h"
#include "image.h"

#define BIT_COUNT 24

#define BMP_BF_TYPE 0x4D42
#define BMP_BI_SIZE 40
#define BI_RGB 0

struct bmp_header {
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
		uint32_t biWidth;
		uint32_t biHeight;
		uint16_t biPlanes;
		uint16_t biBitCount;
		uint32_t biCompression;
		uint32_t biSizeImage;
		uint32_t biXPelsPerMeter;
		uint32_t biYPelsPerMeter;
		uint32_t biClrUsed;
		uint32_t biClrImportant;
} __attribute__((packed));

static uint8_t calculate_padding(const uint32_t image_width) {
	return (4 - sizeof(struct pixel) * image_width % 4) % 4;
}

static void destroy_image(struct image* image_ptr) {
	free_image(image_ptr);
	image_ptr->data = NULL;
}

//Input

static struct pixel* image_row_addr(size_t row, const struct image* image_ptr) {
	return &image_ptr->data[row * image_ptr->width];
}

static enum read_status bmp_header_validate(struct bmp_header header) {
	if (header.bfType != BMP_BF_TYPE)
		return READ_INVALID_SIGNATURE;

	if (header.biBitCount != BIT_COUNT)
		return READ_INVALID_BITS;
		
	if (header.biWidth < 0)
		return READ_INVALID_WIDTH;
		
	if (header.biHeight < 0)
		return READ_INVALID_HEIGHT;
		
	return READ_OK;
}

static int image_row_read(size_t row, struct image* image_ptr, FILE* image_file) {
	return file_read(image_row_addr(row, image_ptr), sizeof(struct pixel), image_ptr->width, image_file);
}

static enum read_status read_image_data(struct image* image_ptr, FILE* image_file) {
	const uint64_t width = image_ptr->width;
	const uint64_t height = image_ptr->height;

	const uint8_t padding = calculate_padding(width);
	
	for (size_t i = 0; i < height; ++i) {
		if ( image_row_read(i, image_ptr, image_file) != width ) {
			destroy_image(image_ptr);
		
			return READ_ERROR;
		}	
		
		if ( file_seek(image_file, padding, SEEK_CUR) ) {
			destroy_image(image_ptr);
		
			return READ_ERROR;
		}
	}
	
	return READ_OK;
}

enum read_status from_bmp(FILE* img, struct image* image_ptr) {
	if (img == NULL)
		return READ_INVALID_FILE;
	else if (image_ptr == NULL)
		return READ_UNKNOWN_ERROR;

	struct bmp_header header = {0};
	
	if (!file_read(&header, sizeof(struct bmp_header), 1, img))
		return READ_INVALID_HEADER;
		
	enum read_status r_status = bmp_header_validate(header);
		
	if (r_status != READ_OK)
		return r_status;
	
	*image_ptr = create_image(header.biWidth, header.biHeight);
	
	return read_image_data(image_ptr, img);
}

//Output

static struct bmp_header create_header(const uint64_t width, const uint64_t height) {
	const uint8_t padding = calculate_padding(width);
	
	const size_t pixel_data_size = sizeof(struct pixel) * width * height;

	struct bmp_header bmp_header = {0};
	
	bmp_header.bfType = BMP_BF_TYPE;
	bmp_header.bfileSize = sizeof(bmp_header) + pixel_data_size + padding * height;
	bmp_header.bfReserved = 0;
	bmp_header.bOffBits = sizeof(bmp_header);
	
	bmp_header.biSize = BMP_BI_SIZE;
	bmp_header.biWidth = width;
	bmp_header.biHeight = height;
	bmp_header.biPlanes = 1;
	bmp_header.biBitCount = sizeof(struct pixel) * 8;
	bmp_header.biCompression = BI_RGB;
	bmp_header.biSizeImage = pixel_data_size;
	bmp_header.biXPelsPerMeter = 0;
	bmp_header.biYPelsPerMeter = 0;
	bmp_header.biClrUsed = 0;
	bmp_header.biClrImportant = 0;
	
	return bmp_header;
}

enum write_status to_bmp(FILE* img, struct image const* image_ptr) {
	const uint64_t width = image_ptr->width;
	const uint64_t height = image_ptr->height;

	struct bmp_header bmp_header = create_header(width, height);
	
	if (file_write(&bmp_header, sizeof(bmp_header), 1, img) < 1)
		return WRITE_ERROR;
	
	const uint8_t padding = calculate_padding(width);
	uint32_t padding_byte = 0;
	
	for (size_t i = 0; i < height; ++i) {
		if (file_write(image_row_addr(i, image_ptr), sizeof(struct pixel), width, img) < width)
			return WRITE_ERROR;
			
		if (file_write(&padding_byte, padding, 1, img) < 1)
			return WRITE_ERROR;
	}
	
	return WRITE_OK;
}
