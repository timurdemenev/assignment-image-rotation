#include "util.h"

#define BIT_COUNT_STR "24"

static const char* const image_read_messages[] = {
	[READ_OK] = "Reading successfully finished!",
	[READ_ERROR] = "Error while reading file",
	[READ_INVALID_FILE] = "Reading from invalid file",
	[READ_INVALID_HEADER] = "Image has invalid bmp header",
	[READ_INVALID_BITS] = ("Image can be only " BIT_COUNT_STR " bits"),
	[READ_INVALID_SIGNATURE] = "Image has invalid bmp signature",
	[READ_INVALID_WIDTH] = "Image has invalid width",
	[READ_INVALID_HEIGHT] = "Image has invalid height",
	[READ_UNKNOWN_ERROR] = "Unknown error while reading image"
};

static const char* const image_write_messages[] = {
	[WRITE_OK] = "Writing to file successfuly finished!",
	[WRITE_ERROR] = "Writing image failed",
	[WRITE_UNKNOWN_ERROR] = "Unknown error while writing image"
};

static size_t normalize(size_t x, size_t def_value, size_t min, size_t max) {
	return x >= min && x <= max ? x : def_value;
}

const char* get_message_for_image_read_status(enum read_status status) {
	return image_read_messages[normalize(status, READ_UNKNOWN_ERROR, 0, READ_UNKNOWN_ERROR)];
}

const char* get_message_for_image_write_status(enum write_status status) {
	return image_write_messages[normalize(status, WRITE_UNKNOWN_ERROR, 0, WRITE_UNKNOWN_ERROR)];
}
