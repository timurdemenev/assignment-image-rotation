#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>

#include "bmp.h"

#include "file.h"

#include "image.h"

#include "rotate_transformer.h"
#include "util.h"

#define FAILED_OPENING_INPUT_FILE_EXIT_CODE 1
#define READ_ERROR_EXIT_CODE 2
#define FAILED_OPENING_OUTPUT_FILE_EXIT_CODE 3
#define WRITE_ERROR_EXIT_CODE 4
#define STANDARD_EXIT_CODE 0

static int read_image_from_bmp_file(char* filepath, struct image* img_ptr) {
	FILE* file = file_open(filepath, "rb");
	
	if (file == NULL) {
		fprintf(stderr, "Error while read source file %s\n", filepath);
		return FAILED_OPENING_INPUT_FILE_EXIT_CODE;
	}
			
	enum read_status r_status = from_bmp(file, img_ptr);
	const char* message = get_message_for_image_read_status(r_status);
	
	int exitcode = 0;
	
	if (r_status != READ_OK) {
		free_image(img_ptr);
		
		exitcode = STANDARD_EXIT_CODE;
	}
	
	file_close(file);
	fprintf(stderr, "%s\n", message);
	
	return exitcode;
}

static int write_image_to_bmp_file(char* filepath, struct image* img_ptr) {
	FILE* output_file = file_open(filepath, "wb");
	
	if (output_file == NULL) {
		fprintf(stderr, "Error while open output file %s\n", filepath);
		free_image(img_ptr);
		
		return FAILED_OPENING_OUTPUT_FILE_EXIT_CODE;
	}
	
	enum write_status w_status = to_bmp(output_file, img_ptr);
	const char* message = get_message_for_image_write_status(w_status);
		
	int exitcode = w_status != WRITE_OK ? WRITE_ERROR_EXIT_CODE : STANDARD_EXIT_CODE;
		
	file_close(output_file);
	free_image(img_ptr);
	
	fprintf(stderr, "%s\n", message);
	
	return exitcode;
}

int main(int argc, char** argv) {
	if (argc != 3) {
		if (argc < 3)
			fprintf(stderr, "Not enough arguments for the program!\n");
		else
			fprintf(stderr, "Too many arguments for the program!\n");
			
		fprintf(stderr, "Usage: %s <source-image> <destination-image>\n", argv[0]);
	}
	
	char* src_file = argv[1];
	char* dst_file = argv[2];
	
	struct image src_img = {0};
	
	int exitcode = read_image_from_bmp_file(src_file, &src_img);
	
	if (exitcode != STANDARD_EXIT_CODE)
		return exitcode;
	
	struct image dst_img = rotate_transform(src_img);
	
	free_image(&src_img);
	
	exitcode = write_image_to_bmp_file(dst_file, &dst_img);

	return exitcode;
}
