#include "file.h"

#include <stdio.h>

FILE* file_open(char* file_name, char* props) {
	return fopen(file_name, props);
}

void file_close(FILE* file) {
	fclose(file);
}

int file_seek(FILE* file, long int offset, int origin) {
	return fseek(file, offset, origin);
}
